﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace Automation_of_BAI2_file_upload
{
    class WebService
    {
       // List<string> listOfTextFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["archiveFilePath"]));

        public string FileUploadtoUCM(string li)
        {               
            
            var _url = ConfigurationSettings.AppSettings["URI"];
            var _action = ConfigurationSettings.AppSettings["ActionUpload"];

            string result = string.Empty;
            string content = string.Empty;

            string xmlCreateURLAttach;

            //string ArchievePath = ConfigurationManager.AppSettings["TextFileFolderPath"] + "CF97382.RAWDATA.20180504" + ".txt";

            using (FileStream fs = new FileStream(li, FileMode.Open, FileAccess.Read))
            {
                byte[] filebytes = new byte[fs.Length];
                fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
                content = Convert.ToBase64String(filebytes);
            }

            string parameter = CreateURLAttachmentParameters(content, li);

            try
            {
                XmlDocument contentxml = new XmlDocument();

                contentxml.LoadXml(parameter);

                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(contentxml, webRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = rd.ReadToEnd();
                        Regex regex = new Regex("<?(.*?)</env:Envelope>");
                        string v = regex.Match(soapResult).Groups[1].ToString();
                        string xmldoccontent = "<" + v + "</env:Envelope>";
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xmldoccontent);

                        XmlNodeList lst = xmldoc.GetElementsByTagName("result");
                        result = lst.Item(0).InnerText;
                   }
                }
            }
            catch (WebException e)
            {
                //pro.SendFailureMail("Journal Import Failed ", "Failed during Upload to UCM" + e, "");
                Console.WriteLine("Error: " + Environment.NewLine + e);
                result = "error";
            }
            catch (Exception ex)
            {
                // pro.SendFailureMail("Journal Import Failed", "Failed during Upload to UCM" + ex, "");
                Console.WriteLine("Error: " + Environment.NewLine + ex);
                result = "error";
            }

            return result;
        }

        private HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["_UserName"], ConfigurationSettings.AppSettings["_Password"]);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            webRequest.Timeout = 240000;
            return webRequest;
        }

        private void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        public static string CreateURLAttachmentParameters(string content, string li)
        {
            StringBuilder newXML_ = new StringBuilder();

            newXML_.Append("<?xml version='1.0' encoding='UTF-8'?>");
            newXML_.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
               newXML_.Append("<soap:Body>");
               newXML_.Append("<ns1:uploadFileToUcm xmlns:ns1='http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/'>");
               newXML_.Append("<ns1:document xmlns:ns2='http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/'>");
                            newXML_.Append("<ns2:Content>" + content);
                            newXML_.Append("</ns2:Content>");
                            newXML_.Append("<ns2:FileName>" + Path.GetFileName(li) + "</ns2:FileName>");
                            newXML_.Append("<ns2:ContentType>text</ns2:ContentType>");
                            newXML_.Append("<ns2:DocumentTitle>Journal Import</ns2:DocumentTitle>");
                            newXML_.Append("<ns2:DocumentAuthor>govind</ns2:DocumentAuthor>	");
                          newXML_.Append("<ns2:DocumentSecurityGroup>FAFusionImportExport</ns2:DocumentSecurityGroup>");
                        newXML_.Append("<ns2:DocumentAccount>fin$/cashManagement$/import$</ns2:DocumentAccount> ");
                     newXML_.Append("</ns1:document>");
                  newXML_.Append("</ns1:uploadFileToUcm>");
               newXML_.Append("</soap:Body>");
            newXML_.Append("</soap:Envelope>");

            return newXML_.ToString();
        }

        public string MoveDatatoInterfaceTables(string fileuploadresult)
        {
            var _url = ConfigurationSettings.AppSettings["URI"];
            var _action = ConfigurationSettings.AppSettings["ActionSubmitProcess"];

            string result = string.Empty;

            try
            {
                XmlDocument contentxml = new XmlDocument();
                string content = GetInterfaceStartParameter() + fileuploadresult + GetInterfaceEndParameter();
                contentxml.LoadXml(content);

                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(contentxml, webRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = rd.ReadToEnd();
                        Regex regex = new Regex("<?(.*?)</env:Envelope>");
                        string v = regex.Match(soapResult).Groups[1].ToString();
                        string xmldoccontent = "<" + v + "</env:Envelope>";
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xmldoccontent);

                        XmlNodeList lst = xmldoc.GetElementsByTagName("result");
                        result = lst.Item(0).InnerText;
                    }
                }

            }
            catch (WebException e)
            {
                //pro.SendFailureMail("Journal Import Failed during submission of Interface table load process", "Failed while moving to Interface Tables" + e, "");
                result = "error";

            }
            catch (Exception ex)
            {
                //pro.SendFailureMail("Journal Import Failed during submission of Interface table load process", "Failed while moving to Interface Tables " + ex, "");
                result = "error";

            }
            return result;
        }


        public string MoveDatatoBaseTablesCA(string interfaceLoadResult)
        {
            var _url = ConfigurationSettings.AppSettings["URI"];
            var _action = ConfigurationSettings.AppSettings["ActionSubmitProcess"];

            string result = string.Empty;
            //Processint pro = new Processint();
            try
            {
                XmlDocument contentxml = new XmlDocument();
                string content = GetBaseStartParameter() + GetCABaseParameters() + GetBaseEndParameter();
                contentxml.LoadXml(content);

                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(contentxml, webRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = rd.ReadToEnd();
                        Regex regex = new Regex("<?(.*?)</env:Envelope>");
                        string v = regex.Match(soapResult).Groups[1].ToString();
                        string xmldoccontent = "<" + v + "</env:Envelope>";
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xmldoccontent);

                        XmlNodeList lst = xmldoc.GetElementsByTagName("result");
                        result = lst.Item(0).InnerText;
                    }
                }
            }
            catch (WebException e)
            {
                //pro.SendFailureMail("Journal Import Failed during submission of Base table load process", "Failed while moving to Base Tables" + e, "");
                result = "error";
                
            }
            catch (Exception ex)
            {
                //pro.SendFailureMail("Journal Import Failed during submission of Base table load process", "Failed while moving to Base Tables " + ex, "");
                result = "error";
                
            }
            return result;
        }

        public void GetJobStatus(string processId, out string jobResult)
        {
            var _url = ConfigurationSettings.AppSettings["URI"];
            var _action = ConfigurationSettings.AppSettings["ActionGetStatus"];

            string result = string.Empty;

            XmlDocument contentxml = new XmlDocument();
            string content = GetJobStatusStartParameter() + processId + GetJobStatusEndParameter();
            contentxml.LoadXml(content);

            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(contentxml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            string soapResult;
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                    Regex regex = new Regex("<?(.*?)</env:Envelope>");
                    string v = regex.Match(soapResult).Groups[1].ToString();
                    string xmldoccontent = "<" + v + "</env:Envelope>";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xmldoccontent);
                    XmlNodeList lst = xmldoc.GetElementsByTagName("result");
                    jobResult = lst.Item(0).InnerText;
                }
            }
        }

        public void emailLogResult(string body, string _subject)
        {

            try
            {
                #region Email
                //string fromEmail = ConfigurationManager.AppSettings["fromEmail"];   // From where the email is coming from   
                //string fromEmailPass = ConfigurationManager.AppSettings["fromEmailPass"];      // Email Password 
                //string toEmail = ConfigurationManager.AppSettings["toEmail"];      // To where the email is going
                //string subject = _subject;   // Subject of the email


                //int portNum = Convert.ToInt16(ConfigurationManager.AppSettings["portNum"]);  //Convert.ToInt32(ConfigurationManager.AppSettings["portNum"]);
                //string hostName = ConfigurationManager.AppSettings["hostName"];

                /////<summary>
                ///// Creates an instance of SmtpClient called client  
                /////<summary>
                //SmtpClient client = new SmtpClient(hostName, portNum);

                ////client.Port = portNum;

                //client.EnableSsl = true;
                //client.Timeout = 10000;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.Credentials = new NetworkCredential(fromEmail, fromEmailPass);
                ////client.UseDefaultCredentials = false;
                //client.Host = hostName;


                /////<summary>
                ///// Creates an instance of MailMessage called mm 
                /////<summary>
                //MailMessage mm = new MailMessage(fromEmail, toEmail, subject, body);
                ////mm.BodyEncoding = UTF8Encoding.UTF8;
                ////mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                ////mm.To.Add(toEmail);

                //client.Send(fromEmail, toEmail, subject, body);
                ////client.Send(mm);
                //Console.WriteLine("Email sent successfully");
                #endregion
                string fromEmail = ConfigurationManager.AppSettings["fromEmail"];   // From where the email is coming from   
                string fromEmailPass = ConfigurationManager.AppSettings["fromEmailPass"];      // Email Password 
                string toEmail = ConfigurationManager.AppSettings["toEmail"];      // To where the email is going
                string subject = _subject;   // Subject of the email


                int portNum = Convert.ToInt16(ConfigurationManager.AppSettings["portNum"]);  //Convert.ToInt32(ConfigurationManager.AppSettings["portNum"]);
                string hostName = ConfigurationManager.AppSettings["hostName"];

                ///<summary>
                /// Creates an instance of SmtpClient called client  
                ///<summary>
                SmtpClient client = new SmtpClient(hostName, portNum);

                ////************* From Vigilant ****************
                ////client.Port = portNum;

                //client.EnableSsl = true;
                //client.Timeout = 10000;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.Credentials = new NetworkCredential(fromEmail, fromEmailPass);
                ////client.UseDefaultCredentials = false;
                //client.Host = hostName;
                ////*************************************************

                //this if for Leon's - mariam
                //client.Port = portNum;
                client.Host = hostName;
                client.EnableSsl = false;
                client.Timeout = 10000;
                client.UseDefaultCredentials = true;

                ///<summary>
                /// Creates an instance of MailMessage called mm 
                ///<summary>
                MailMessage mm = new MailMessage(fromEmail, toEmail, subject, body);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                mm.To.Add(toEmail);

                client.Send(fromEmail, toEmail, subject, body);
                //client.Send(mm);
                Console.WriteLine("Email sent successfully");

            }
            catch (Exception e)
            {

                Console.WriteLine("Error: Email --- " + e);
                Console.WriteLine("\n\nPress any key to continue... ");

            }
        }

        private string GetInterfaceStartParameter()
        {
            return @"<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:typ=""http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/"">
                    <soapenv:Body>
                    <typ:submitESSJobRequest>
                    <typ:jobPackageName>oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader</typ:jobPackageName>
                      <typ:jobDefinitionName>InterfaceLoaderController</typ:jobDefinitionName>
                      <typ:paramList>5</typ:paramList>
                      <typ:paramList>";
        }

        private string GetInterfaceEndParameter()
        {
            return @"</typ:paramList>
                      <typ:paramList>N</typ:paramList>
                      <typ:paramList>N</typ:paramList>
                      </typ:submitESSJobRequest>
                  </soapenv:Body>
                </soapenv:Envelope>";
        }

        private string GetBaseStartParameter()
        {
            return @"<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:typ='http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/'>
                  <soapenv:Body>
                    <typ:submitESSJobRequest>
                    <typ:jobPackageName>/oracle/apps/ess/financials/generalLedger/programs/common</typ:jobPackageName>
                      <typ:jobDefinitionName>JournalImportLauncher</typ:jobDefinitionName>
                      <typ:paramList>";
        }

        private string GetBaseEndParameter()
        {
            return @"</typ:paramList>
                      <typ:paramList>N</typ:paramList>
                      <typ:paramList>N</typ:paramList>
                      <typ:paramList>N</typ:paramList>
                    </typ:submitESSJobRequest>
                  </soapenv:Body>
                </soapenv:Envelope>";
        }

        private string GetCABaseParameters()
        {
            return @"300000001309020</typ:paramList>
                  <typ:paramList>Spreadsheet</typ:paramList>
                  <typ:paramList>300000001309005</typ:paramList>
                  <typ:paramList>ALL";
        }

        private string GetJobStatusStartParameter()
        {
            return @"<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
                  <soap:Body>
                    <ns1:getESSJobStatus xmlns:ns1='http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/'>
                      <ns1:requestId>";
        }

        private string GetJobStatusEndParameter()
        {
            return @"</ns1:requestId>
                    </ns1:getESSJobStatus>
                  </soap:Body>
                </soap:Envelope>";
        }
    }
}
