﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Net;
using WinSCP;
using System.IO.Compression;


namespace Automation_of_BAI2_file_upload
{
    class Program
    {
        public static List<string> serverList = new List<string>();
        public static List<string> bankAccountNumberList = new List<string>();
        public static List<string> instanceBankAccountNumList = new List<string>();

        public static List<string> FoundBankAccountNumList = new List<string>();
        public static List<string> otherBankAccountNumList = new List<string>();

        public static List<string> listOfFiles;
        public static List<string> listOfTextFiles;

        static void Main(string[] args)
        {

            WebService ws = new WebService();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "====================================================================================================="));
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Start Program"));

            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Check for Bank Accounts List"));
            instanceBankAccountList();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Completed - Checking the Bank Accounts List"));

            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Start Downloading BAI2 files from CIBC Server"));
            downloadFileFromRemotePath();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Completed - Downloading the BAI2 files from CIBC Server"));

            // Find Bank Accounts
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Begin Function for Find Bank Account Numbers"));
            findBankAccountNumber();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Completed function for Finding Bank Account Numbers"));

            // Move Files into folders
            moveFileToProcessedFileFolder();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "File Moved to Processed Folder"));

            moveFileToArchiveFileFolder();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "File Moved to Archive folder"));

            listOfTextFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["archiveFilePath"]));

            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Uploading to UCM and Loading to Interface..."));
            Console.WriteLine("Uploading to UCM and Loading to Interface...");
            foreach (string li in listOfTextFiles)
            {
                string fileUploadResult = ws.FileUploadtoUCM(li);

                //Load Interface

                LoadInterface(fileUploadResult, ws);
            }

            MoveFileToBackUpFolder();
            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Program Execution Completed"));
            Console.WriteLine("Program Finished,\n Press Any Key To Close...");
        }

        //List of all bank accounts used in 
        #region Bank Account List
        public static void instanceBankAccountList()
        {
            instanceBankAccountNumList.Add("000495954321");
            instanceBankAccountNumList.Add("000020272817");
            instanceBankAccountNumList.Add("000028380511");
            instanceBankAccountNumList.Add("049596665217");
            instanceBankAccountNumList.Add("049596665012");
            instanceBankAccountNumList.Add("049596664911");
            instanceBankAccountNumList.Add("000028336415");
            instanceBankAccountNumList.Add("000025500311");
            instanceBankAccountNumList.Add("000026801013");
            instanceBankAccountNumList.Add("000025500117");
            instanceBankAccountNumList.Add("049596648614");
            instanceBankAccountNumList.Add("049590387118");
            instanceBankAccountNumList.Add("000020347213");
            instanceBankAccountNumList.Add("000495912345");
        }
        #endregion


        // Downloading the files from the remmote server
        #region Download File From Remote Path
        public static void downloadFileFromRemotePath()
        {
            SessionOptions _sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = ConfigurationManager.AppSettings["Host"],
                UserName = ConfigurationManager.AppSettings["Username"],
                SshPrivateKeyPath = ConfigurationManager.AppSettings["LFLKey"],
                GiveUpSecurityAndAcceptAnySshHostKey = true,
                Timeout = new TimeSpan(0, 0, 200)
            };

            using (Session _session = new Session())
            {
                try
                {
                    // Connection   
                    _session.ExecutablePath = ConfigurationManager.AppSettings["WinSCPExecutable"];
                    _session.Open(_sessionOptions);

                    // Uploading the files
                    TransferOptions _transferOpt = new TransferOptions();
                    _transferOpt.TransferMode = TransferMode.Binary;

                    TransferOperationResult _transferOperationResult;


                    //================== Changes ====================
                    var sourcePath = RemotePath.EscapeFileMask(ConfigurationManager.AppSettings["remoteTestingPath"]);
                    

                    string check;// = System.IO.Directory.GetFiles(sourcePath);
                    RemoteDirectoryInfo directory = _session.ListDirectory(sourcePath);
                    foreach (RemoteFileInfo filelist in directory.Files)
                    {
                        check = filelist.ToString();
                       
                        //if(check.Substring(0,2) == "CF")
                        if (check.Substring(0,2) == "CF") {

                            //string TodayDate = DateTime.Today.AddDays(-3).ToString("yyyyMMdd");
                            //string fileDate = check.Split('.')[2];

                            //if (fileDate == TodayDate)
                            //{
                            //    Console.WriteLine(filelist.Name);
                            //    _transferOperationResult = _session.GetFiles(sourcePath + filelist.Name, ConfigurationManager.AppSettings["localPath"], false, _transferOpt);
                            //}

                            Console.WriteLine(filelist.Name);
                            LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, filelist.Name));
                            _transferOperationResult = _session.GetFiles(sourcePath + filelist.Name, ConfigurationManager.AppSettings["localPath"], false, _transferOpt);
                        }                  
                    }
                   
                }
                catch (Exception e)
                {
                    LogWriter.WriteLog(String.Format("{0} @ {1}", DateTime.Now, "Exception encountered while downloading BAI2 files from the CIBC server: " + e));
                    Console.WriteLine("Error --- " + e);
                } 
            }
        }
        #endregion


        // Move File to Processed File Folder
        #region Move File to Processed Folder

        public static void moveFileToProcessedFileFolder()
        {
            foreach (string li in listOfFiles)
	        {
                //Console.WriteLine(li);
		        string proPath = ConfigurationManager.AppSettings["processedFilePath"] + Path.GetFileName(li);
                string locPath = ConfigurationManager.AppSettings["localPath"] + Path.GetFileName(li);
                File.Copy(locPath, proPath, false);
	        }    
        }
        #endregion

        // Convert Processed File to Text and move them to the Archieve Folder
        #region Move File to Achive Folder
 
        public static void moveFileToArchiveFileFolder()
        {
            foreach (string li in listOfFiles)
            {
                string proPath = ConfigurationManager.AppSettings["processedFilePath"] + Path.GetFileName(li);
                string arcPath = ConfigurationManager.AppSettings["archiveFilePath"] + Path.GetFileName(li);
                //File.Copy(proPath, arcPath, false);

                #region Convert To Text File
                string content = string.Empty;

                using (FileStream fs = new FileStream(proPath, FileMode.Open, FileAccess.Read))
                {
                    byte[] filebytes = new byte[fs.Length];
                    fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));

                    File.WriteAllBytes(arcPath +  ".txt", filebytes);
                }
                #endregion
            }
        }
        #endregion

        //Find Bank Account -- This Might Not Be Needed Anymore
        public static void findBankAccountNumber()
        {
            //== Changes ==
            listOfFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["localPath"]));

            string input;
            int inListCount = 0;
            int totalCount = 0;
            bool isFound = false;
            string currentDate = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");

            
            foreach (var line in listOfFiles)
            {

                // Change file location back to Processed File Folder 
                using (StreamReader reader = new StreamReader(line))
                {
                    while ((input = reader.ReadLine()) != null && input != "")
                    {
                        if (input.Substring(0, 2) == "03")
                        {
                            foreach (string instAcctNumList in instanceBankAccountNumList)
                            {
                                if (instAcctNumList == input.Substring(3, 12))
                                {
                                    //Console.WriteLine(instAcctNumList);
                                    inListCount++;
                                    FoundBankAccountNumList.Add(instAcctNumList);
                                    isFound = true;
                                }
                            }
                            totalCount++;

                            if (isFound == false)
                            {
                                otherBankAccountNumList.Add(input.Substring(3, 12));
                            }

                            isFound = false;
                        }
                    }
                }
                    //== Changes ==
                if (totalCount != inListCount)
                {
                    File.Delete(line);
                }

                otherBankAccountNumList.Clear();
                FoundBankAccountNumList.Clear();


                inListCount = 0;
                totalCount = 0;
            }
            //== Changes ==
            listOfFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["localPath"]));
          //  Console.ReadKey();
        }

        public static void LoadInterface(string fileUploadResult, WebService ws)
        {
            string interfaceLoadResult = string.Empty;
            string jobResult = string.Empty;

            if (fileUploadResult != "error")
            {
                interfaceLoadResult = ws.MoveDatatoInterfaceTables(fileUploadResult);

                try
                {
                    if (interfaceLoadResult != "error")
                    {

                        while (jobResult.ToUpper() != "SUCCEEDED")
                        {
                            ws.GetJobStatus(interfaceLoadResult, out jobResult);

                            if (
                                    jobResult.ToUpper() == "WAIT" ||
                                    jobResult.ToUpper() == "RUNNING" ||
                                    jobResult.ToUpper() == "READY" ||
                                    jobResult.ToUpper() == "PAUSED" ||
                                    jobResult.ToUpper() == "COMPLETED"
                              )
                            {

                                System.Threading.Thread.Sleep(60000);

                            }
                            else if (jobResult.ToUpper() == "ERROR")
                            {
                                Console.WriteLine("interfaceLoadResult: Error - There may be issues within the file that was loaded.");

                                string body = "Error: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\ninterfaceLoadResult: Error - There may be issues within the file that was loaded.";

                                ws.emailLogResult(body, "Error has occured");
                                break;
                            }
                            else if (jobResult.ToUpper() == "WARNING")
                            {
                                Console.WriteLine("interfaceLoadResult: Error - moving data to the interface table.");

                                string body = "Error: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\nError - There may be issues within the file that was loaded.";

                                ws.emailLogResult(body, "Error - ");

                                break;
                            }

                        }

                        if (jobResult.ToUpper() == "SUCCEEDED")
                        {
                            Console.WriteLine("interfaceLoadResult: Successfully loaded to Oracle");
                            string body = "Successfully Loaded: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\nSuccessfully loaded to Oracle request ID.";

                            ws.emailLogResult(body, "Successfully loaded");

                            #region ExtraCode
                            //p_newfilename = p_success + p_filewithoutextension + DateTime.Now.ToString("MMddyyyyHHmm") + p_extension;
                            //File.Move(ConfigurationSettings.AppSettings["JournalFileName"], p_newfilename);
                            //attachfilename = downloadLogfile(basetableLoadResult);
                            //if (attachfilename == null)
                            //{
                            //    SendSuccessMail(type + " Successfully loaded to Oracle request ID:" + basetableLoadResult, "Journal Import Request ID: " + basetableLoadResult + " Failed to download log files", "");
                            //}
                            //else
                            //{
                            //    SendSuccessMail(type + " Successfully loaded to Oracle request ID:" + basetableLoadResult, "Journal Import Request ID: " + basetableLoadResult, attachfilename);
                            //}
                            #endregion
                        }
                    }
                }
                catch (WebException e)
                {
                    Console.WriteLine("interfaceLoadResult: Error - moving data to the interface table.");

                    string body = "Error: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\nError: moving data to the interface table.";

                    ws.emailLogResult(body, "Error has occured");
                                    
                }
                catch (UnauthorizedAccessException ex)
                {
                    Console.WriteLine("interfaceLoadResult: Error - moving data to the interface table.");

                    string body = "Error: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\nError: moving data to the interface table.";

                    ws.emailLogResult(body, "Error has occured");
                                    
                }
                catch (Exception e1)
                {
                    Console.WriteLine("interfaceLoadResult: Error - moving data to the interface table.");

                    string body = "Error: \n" + DateTime.Now.ToString("YYYY/MM/DD (HH:MM tt)") + "\n\nError: moving data to the interface table.";

                    ws.emailLogResult(body, "Error has occured");
                                   
                }

            }
        }

        // Move Files to Back Up Folder
        public static void MoveFileToBackUpFolder()
        {
            List<string> oldProcessingFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["processedFilePath"]));
            string backUpFolderProcessingFile = ConfigurationManager.AppSettings["backUpProcessFolder"];
            foreach (string li in oldProcessingFiles)
            {
                //File.Delete(startProcessPath + Path.GetFileName(li));
                File.Move(li, backUpFolderProcessingFile + Path.GetFileName(li));
            }

            List<string> oldArchieveFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["archiveFilePath"]));
            string backUpFolderArchieve = ConfigurationManager.AppSettings["backUpArchieveFolder"]; ;
            foreach (string li in oldArchieveFiles)
            {
                //File.Delete(startProcessPath + Path.GetFileName(li));
                File.Move(li, backUpFolderArchieve + Path.GetFileName(li));
            }

            List<string> originalFiles = new List<string>(Directory.EnumerateFiles(ConfigurationManager.AppSettings["localPath"]));
            string backUpFolderOriginal = ConfigurationManager.AppSettings["BackUpOriginalFileFolder"]; ;
            foreach (string li in originalFiles)
            {
                //File.Delete(startProcessPath + Path.GetFileName(li));
                File.Move(li, backUpFolderOriginal + Path.GetFileName(li));
            }

        }
    }
}
